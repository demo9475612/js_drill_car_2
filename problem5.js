let inventory = require("./js_drill_2");
const correctSalaries = require("./problem3");

function sumOfSalariesByCountry(){
    inventory = correctSalaries();
    let sumByCountry = {};
    for(let id=0;id<inventory.length;id++){
        let country=inventory[id].location;
        if(!sumByCountry[country]){
           sumByCountry[country]=0;
        }
        sumByCountry[country]+=inventory[id].corrected_salary;
    }
    return sumByCountry;
}
module.exports=sumOfSalariesByCountry;