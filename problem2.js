const inventory = require("./js_drill_2");

function convertSalariesToNumber(){
    if(!Array.isArray(inventory)){
        throw new Error('Invalid inventory Found');
    }
    for(let id=0;id<inventory.length;id++){
        let salaryString=inventory[id].salary;
        inventory[id].salary=parseFloat(salaryString.substring(1,salaryString.length));
        
    }
    return inventory;
}
module.exports=convertSalariesToNumber;