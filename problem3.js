let inventory = require("./js_drill_2");
const convertSalariesToNumber = require("./problem2");

function correctSalaries(){
    if(!Array.isArray(inventory)){
        throw new Error('Invalid inventory Found');
    }
    inventory = convertSalariesToNumber();
    for(let id=0;id<inventory.length;id++){
        let correctSalary = (inventory[id].salary)*10000;
        inventory[id].corrected_salary = correctSalary;
    }
    return inventory;
}
module.exports= correctSalaries;