let inventory = require("./js_drill_2");
const correctSalaries = require("./problem3");

function averageSalaryByCountry() {
    inventory = correctSalaries();
    let averageByCountry = {};
    let countByCountry = {};
    for (let i = 0; i < inventory.length; i++) {
        let country = inventory[i].location;
        if (!countByCountry[country]) {
            countByCountry[country] = 1;
            averageByCountry[country] = inventory[i].corrected_salary;
        } else {
            countByCountry[country]++;
            averageByCountry[country] += inventory[i].corrected_salary;
        }
    }

    for (let country in averageByCountry) {
        averageByCountry[country] /= countByCountry[country];
    }

    return averageByCountry;
}

module.exports = averageSalaryByCountry;